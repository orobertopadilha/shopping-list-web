import React from "react"
import { v4 } from 'uuid'

class App extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            items: [],
            amount: '',
            product: '',
            brand: ''
        }
    }

    componentDidMount() {
        let storageItems = localStorage.getItem('shopping-list')
        if (storageItems != null) {
            this.setState({ items: JSON.parse(storageItems) })
        }
    }

    onInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        })
    }

    addItem = () => {
        let newItem = {
            id: v4(),
            amount: this.state.amount,
            product: this.state.product,
            brand: this.state.brand
        }

        let itemsUpdated = [...this.state.items, newItem]
        this.setState({
            items: itemsUpdated,
            amount: '',
            product: '',
            brand: ''
        })

        localStorage.setItem('shopping-list', JSON.stringify(itemsUpdated))
    }

    render() {
        return (
            <div>
                <h1>Shopping List</h1>

                <form>
                    <input min="1" max="10" type="number" name="amount"
                        value={this.state.amount} onChange={this.onInputChange} />

                    <input type="text" size="30" name="product" placeholder="Produto"
                        value={this.state.product} onChange={this.onInputChange} />

                    <input type="text" size="15" name="brand" placeholder="Marca"
                        value={this.state.brand} onChange={this.onInputChange} />

                    <input type="button" value="Adicionar" onClick={this.addItem} />
                </form>

                <table width="80%" border="1" style={{ marginTop: '20px' }}>
                    <thead>
                        <tr>
                            <th style={{ width: '10%' }}>Qtde.</th>
                            <th style={{ width: '70%' }}>Produto</th>
                            <th style={{ width: '20%' }}>Marca</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.items.map(item =>
                                <tr key={item.id}>
                                    <td style={{ textAlign: 'center' }}>{item.amount}</td>
                                    <td>{item.product}</td>
                                    <td>{item.brand}</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}

export default App